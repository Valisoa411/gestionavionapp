import React, { useEffect, useRef, useState } from 'react';
import { Container, Table } from 'reactstrap';
import { IonButton, IonButtons, IonCol, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonList, IonMenuButton, IonModal, IonRow, IonThumbnail, IonTitle, IonToolbar } from '@ionic/react';

const ListeAvion: React.FC = () => {

    const [list_, setList] = useState<any[]>([]);
    const [loading, setLoading] = useState(false);
    const [login, setLogin] = useState(false);
    const [idVehicule, setIdVehicule] = useState(0);
    var email = useRef<HTMLIonInputElement>(null);
    var mdp = useRef<HTMLIonInputElement>(null);
    
    useEffect(() => {
        setLoading(true);
        fetch('https://gestionavion-production.up.railway.app/avions')
            .then(data => data.json())
            .then(res => {
                setList(res.data.data.listavion);
                setLoading(false);
            })
    }, [])

    var xhr = new XMLHttpRequest();
    function verifyLogin(email: any, mdp: any) {
        xhr.onreadystatechange = function () {
            console.log(xhr.readyState, xhr.status);
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var response = JSON.parse(xhr.responseText);
                    if (response.data.message = "Login Success") {
                        window.location.href = "/avions/" + idVehicule + "/" + response.data.data.token;
                    }
                    else {
                        alert("Email ou mot de passe incorrect. Veuillez reessayer!");
                    }
                } else {
                    alert("Une erreur s'est produite, veuillez réessayer");
                }
            }
        };
        xhr.open("POST", "https://gestionavion-production.up.railway.app/login?login=" + email + "&&mdp=" + mdp);
        xhr.send(null);
        console.log(email, mdp);
    }

    const TraitementLogin = () => {
        verifyLogin(email.current?.value, mdp.current?.value);
    };

    function voirDetail(id: any) {
        setLogin(true);
        setIdVehicule(id);
    };

    function ConfirmationLogin(){
        return (
            <IonModal isOpen={login}>
                <IonHeader>
                    <IonToolbar color='primary'>
                        <IonTitle>Authentification</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent>
                    <div id="login">
                        <form>
                            <IonItem>
                                <IonLabel position="floating"> Nom</IonLabel>
                                <IonInput ref={email} value="user@gmail.com"></IonInput>
                            </IonItem>
                            <IonItem>
                                <IonLabel position="floating">Mot de passe</IonLabel>
                                <IonInput type="password" ref={mdp} value="user"></IonInput>
                            </IonItem>
                            <IonRow>
                                <IonCol className="ion-text-center">
                                    <IonButton onClick={TraitementLogin}>Se connecter</IonButton>
                                </IonCol>
                            </IonRow>
                            <span id="erreur"></span>
                        </form>
                    </div >
                </IonContent>
            </IonModal>           
        );
    }

    const allVehicule = list_.map(group => {
        console.log("Test");
        return (
            <IonItem>
                <IonThumbnail slot="start">
                    <img alt="Silhouette of mountains"
                        src="https://ionicframework.com/docs/img/demos/thumbnail.svg" />
                </IonThumbnail>
                <IonLabel>
                    {group.marque}
                </IonLabel>
                <IonLabel>
                    {group.immatriculation}
                </IonLabel>
                <IonItem>
                    <IonButton color="success" fill='outline' onClick={() => voirDetail(group.idAvion)}>Voir details</IonButton>
                </IonItem>
            </IonItem>        
        )
    })

    function VehiculeList() {
        return (
            <IonContent>
                <div>
                    <Container fluid>
                        <h3>Liste des avions

                        </h3>
                        <Table className="mt-4" border={1} width={600}>
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Marque</th>
                                    <th>Immatriculation</th>
                                </tr>
                            </thead>
                            <tbody>
                                {allVehicule}
                            </tbody>
                        </Table>
                    </Container>
                </div>
                <ConfirmationLogin />
            </IonContent>
        );
    }

    function ListAllVehicule(){
        return (
            <>
                <IonItem lines="none">
                    <IonButtons slot="start">
                        <IonMenuButton></IonMenuButton>
                    </IonButtons>
                    <IonTitle>Liste des avions </IonTitle>
                </IonItem>
                <IonList>
                    {allVehicule}
                </IonList>
                <ConfirmationLogin />
            </>
        );
    }
    
    return (
        <ListAllVehicule />
        // <VehiculeList />
    );
};

export default ListeAvion;