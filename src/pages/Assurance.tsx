import React, { useEffect, useRef, useState } from 'react';
import { Container, Table } from 'reactstrap';
import { IonButton, IonButtons, IonCol, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonList, IonMenuButton, IonModal, IonRow, IonSelect, IonSelectOption, IonThumbnail, IonTitle, IonToolbar } from '@ionic/react';

const Assurance: React.FC = () => {

    const [list_, setList] = useState<any[]>([]);
    const [loading, setLoading] = useState(false);
    var mois = useRef<HTMLIonSelectElement>(null);

    var xhr = new XMLHttpRequest();
    function showListe() {
        alert("Check");
        xhr.onreadystatechange = function () {
            console.log(xhr.readyState, xhr.status);
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var response = JSON.parse(xhr.responseText);
                    alert(response);
                    setList(response);
                } else {
                    alert("Une erreur s'est produite, veuillez réessayer");
                }
            }
        };
        xhr.open("POST", "https://gestionavion-production.up.railway.app/assurances/expired/" + mois.current?.value);
        xhr.send(null);
    }

    const allVehicule = list_.map(group => {
        console.log("Test");
        return (
            <IonItem>
                <IonThumbnail slot="start">
                    <img alt="Silhouette of mountains"
                        src="https://ionicframework.com/docs/img/demos/thumbnail.svg" />
                </IonThumbnail>
                <IonLabel>
                    {group.marque}
                </IonLabel>
                <IonLabel>
                    {group.immatriculation}
                </IonLabel>
            </IonItem>
        )
    })

    const voirListe = () => {
        showListe();
    };

    function ListAllVehicule() {
        return (
            <>
                <IonItem>
                    <IonLabel position="floating">Expiré dans: </IonLabel>
                    <IonSelect ref={mois} interface="popover" placeholder="Selectionnez ici" onChange={voirListe}>
                        <IonSelectOption value="1">1 mois</IonSelectOption>
                        <IonSelectOption value="3">3 mois</IonSelectOption>
                    </IonSelect>
                </IonItem>
                <IonItem lines="none">
                    <IonButtons slot="start">
                        <IonMenuButton></IonMenuButton>
                    </IonButtons>
                    <IonTitle>Liste des avions</IonTitle>
                </IonItem>
                <IonList>
                    {allVehicule}
                </IonList>
            </>
        );
    }

    return (
        <ListAllVehicule />
    );
};

export default Assurance;