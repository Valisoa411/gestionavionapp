import React from 'react';
import {
    IonButtons,
    IonContent,
    IonHeader,
    IonItem,
    IonLabel,
    IonList,
    IonMenu,
    IonMenuButton,
    IonPage,
    IonTitle,
    IonToolbar
} from '@ionic/react';

import './Menu.css';

function ListeMenu() {
    return (
        <IonList>
            <IonItem href='#'>
                <IonLabel>Vehicule</IonLabel>
            </IonItem>
            <IonItem href='#'>
                <IonLabel>Kilometrage</IonLabel>
            </IonItem>
            <IonItem href='#'>
                <IonLabel>Assurance</IonLabel>
            </IonItem>
        </IonList>
    );
};

// function Menu() {
const Menu: React.FC = () => {
    return (
        <>
            <IonMenu contentId="main-content">
                <IonHeader>
                    <IonToolbar color="tertiary">
                        <IonTitle>Menu</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent className="ion-padding">
                    <ListeMenu />
                </IonContent>
            </IonMenu>
            <IonPage id="main-content">
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton></IonMenuButton>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
            </IonPage>
        </>
    );
}
export default Menu;