import React, { useEffect, useState } from 'react';
import { Button, ButtonGroup, Container, Table } from 'reactstrap';
import { Link, useParams } from 'react-router-dom';

const LoginForm: React.FC = () => {
    const params = useParams<{ idVehicule: string }>();
    sessionStorage.setItem("idVehicule", params.idVehicule);
    return (
        <div>
            <Container fluid>
                <h3>Login</h3>
                <form action={'/vehicules/' + params.idVehicule } method='get'>
                    <input type='text' name='login' placeholder='Login'></input>
                    <input type='password' name='mdp' placeholder='Mot de passe'></input>
                    <input type='submit' value='Valider'></input>
                </form>
            </Container>
        </div>
    );
};

export default LoginForm;