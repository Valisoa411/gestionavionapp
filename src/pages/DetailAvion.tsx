import React, { useEffect, useState } from 'react';
import { Container } from 'reactstrap';
import {
    IonBackButton,
    IonBadge,
    IonButton,
    IonButtons,
    IonContent,
    IonHeader,
    IonItem,
    IonLabel,
    IonList,
    IonNote,
    IonPage,
    IonThumbnail,
    IonTitle,
    IonToolbar,
} from '@ionic/react';
import { useParams } from 'react-router';
import { Photo, Camera, CameraResultType } from '@capacitor/camera';

const DetailAvion: React.FC = () => {
    const params = useParams<{ idAvion: string }>();
    const token = useParams<{ token: string }>();
    const [list_, setList] = useState<any>([]);
    const [loading, setLoading] = useState(false);

    const [image, setImage] = useState<Photo | undefined>(undefined);
    const uploadPhoto = async () => {
        setImage(await Camera.getPhoto({
            quality: 90,
            allowEditing: true,
            resultType: CameraResultType.Base64
        }));
    }

    useEffect(() => {
        setLoading(true);
        console.log("token : "+token.token);
        fetch('https://gestionavion-production.up.railway.app/avions/' + params.idAvion + '?token=' + token.token)
            .then(data => data.json())
            .then(res => {
                setList(res);
                console.log("Liste" + list_);
                setLoading(false);
            })
    }, [])

    return (
        <IonPage id="view-message-page">
            <IonHeader>
                <IonToolbar color='primary'>
                    <IonButtons slot="start">
                        <IonBackButton text="Retour" defaultHref="/Vehicule"></IonBackButton>
                    </IonButtons>
                    <IonTitle>Details</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonList>
                    <IonItem>
                        <IonThumbnail slot="start">
                            {image &&
                                <img src={'data:image/' + image.format + ';base64,' + image.base64String} />}
                        </IonThumbnail>
                        <IonItem  >
                            <IonButton onClick={uploadPhoto}> Upload  </IonButton>
                        </IonItem>
                        <IonLabel>
                            <IonBadge color="primary" slot="start">
                                <h2>{list_.immatriculation}</h2>
                            </IonBadge>
                            <h3>Marque : <IonNote>{list_.marque}</IonNote></h3>
                            <h3>DateEXP : <IonNote>2022-12-29</IonNote></h3>
                        </IonLabel>
                    </IonItem>
                </IonList>
            </IonContent>
        </IonPage>
    );
}

export default DetailAvion;
