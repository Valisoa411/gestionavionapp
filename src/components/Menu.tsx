import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
} from '@ionic/react';

import { useLocation } from 'react-router-dom';
import { archiveOutline, archiveSharp, bookmarkOutline, briefcase, briefcaseOutline, briefcaseSharp, carOutline, carSharp, heartOutline, heartSharp, homeOutline, homeSharp, imageOutline, imageSharp, informationCircle, informationCircleOutline, informationSharp, listOutline, listSharp, mailOutline, mailSharp, paperPlaneOutline, paperPlaneSharp, personAddSharp, personOutline, personSharp, serverOutline, serverSharp, trashOutline, trashSharp, warningOutline, warningSharp, waterOutline } from 'ionicons/icons';
import './Menu.css';

interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}

const appPages: AppPage[] = [
  {
    title: 'Liste Vehicule',
    url: '/voiture/ListeVoiture',
    iosIcon: listOutline,
    mdIcon: listSharp
  },
  {
    title: 'Assurance',
    url: '/page/Outbox',
    iosIcon: homeOutline,
    mdIcon: homeSharp
  },
  {
    title: 'Kilometrage',
    url: '/page/Outbox',
    iosIcon: carOutline,
    mdIcon: carSharp
  },
  {
    title: 'Entretien',
    url: '/page/Favorites',
    iosIcon: waterOutline,
    mdIcon: warningSharp
  },

  {
    title: 'About',
    url: '/ficheTechnique/voiture',
    iosIcon: informationCircleOutline,
    mdIcon: informationSharp
  }
];

const labels = ['Family'];

const Menu: React.FC = () => {
  const location = useLocation();

  return (
    <IonMenu contentId="main" type="overlay">
      <IonContent>
        <IonList id="inbox-list">
          <IonListHeader>FY-Art</IonListHeader>
          <IonNote>hi@fy_Art.com</IonNote>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false}>
                  <IonIcon slot="start" ios={appPage.iosIcon} md={appPage.mdIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
        </IonList>

        <IonList id="labels-list">
          <IonListHeader>Labels</IonListHeader>
          {labels.map((label, index) => (
            <IonItem lines="none" key={index}>
              <IonIcon slot="start" icon={bookmarkOutline} />
              <IonLabel>{label}</IonLabel>
            </IonItem>
          ))}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
