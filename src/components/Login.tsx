
import { useRef, useState } from 'react';
// import styled from 'styled-components';
import { IonAvatar, IonButton, IonHeader, IonIcon, IonInput, IonItem, IonItemGroup, IonLabel, IonNote, IonTitle, IonToolbar } from '@ionic/react';
import './Login.css';
import { lockClosed, mail } from 'ionicons/icons';
function Login() {
    const [isTouched, setIsTouched] = useState(false);
    const [isValid, setIsValid] = useState<boolean>();
    var email = useRef<HTMLIonInputElement>(null);
    var mdp = useRef<HTMLIonInputElement>(null);

    const validateEmail = (email: string) => {
        return email.match(
            /^(?=.{1,254}$)(?=.{1,64}@)[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
        );
    };

    const validate = (ev: Event) => {
        const value = (ev.target as HTMLInputElement).value;

        setIsValid(undefined);

        if (value === '') return;

        validateEmail(value) !== null ? setIsValid(true) : setIsValid(false);
    };

    const markTouched = () => {
        setIsTouched(true);
    };

    var xhr = new XMLHttpRequest();
    function verifyLogin(email: any, mdp: any) {
        xhr.onreadystatechange = function () {
            console.log(xhr.readyState, xhr.status);
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var response = JSON.parse(xhr.responseText);
                    if(response.data.message == "Login Success"){
                        window.location.href = "/Avion";
                    }
                    if (response.data.message == null) {
                        alert("Email ou mot de passe incorrect. Veuillez reessayer!");
                    }
                } else {
                    alert("Une erreur s'est produite, veuillez réessayer");
                }
            }
        };
        xhr.open("POST", "https://gestionavion-production.up.railway.app/login?login=" + email + "&&mdp=" + mdp);
        xhr.send(null);
        console.log(email, mdp);
    }

    const TraitementLogin = () => {
        verifyLogin(email.current?.value, mdp.current?.value);
    };

    return (
        <>
            <IonItemGroup className="ion-align-items-center">

                <IonHeader>
                    <IonToolbar >
                    <IonAvatar slot="start">
                        <img alt="Silhouette of a person's head" src="https://ionicframework.com/docs/img/demos/avatar.svg" />
                    </IonAvatar>
                        <IonTitle size="large">Login</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonItem fill="solid" className={`${isValid && 'ion-valid'} ${isValid === false && 'ion-invalid'} ${isTouched && 'ion-touched'}`}>
                    {/* <IonIcon icon={mail} /> */}
                    <IonLabel position="floating">Email</IonLabel>
                    <IonInput ref={email} type="email" clearInput={true} value="user@gmail.com"  placeholder='Enter email adress' onIonInput={(event) => validate(event)} onIonBlur={() => markTouched()}></IonInput>
                </IonItem>
                <IonItem className='ion-align-items-center ion-justify-content-center' fill="solid">
                    {/* <IonIcon icon={lockClosed} /> */}
                    <IonLabel position="floating">Password</IonLabel>
                    <IonInput ref={mdp} type='password' clearInput={true} value="user" placeholder="Enter name"></IonInput>
                </IonItem>
                <IonItem>
                    <IonButton className="ion-margin-top" type="submit" expand="block" onClick={TraitementLogin}>
                    Login
                </IonButton>
                {/* <IonButton routerLink={'/signin/create'} type="submit" expand="block">
                    Create Account
                </IonButton> */}
                </IonItem>
            </IonItemGroup>
        </>
    );
}
export default Login;