
import { useState } from 'react';
// import {Button} from Button';
// import styled from 'styled-components';
import { IonAvatar, IonBackButton, IonButton, IonButtons, IonHeader, IonIcon, IonInput, IonItem, IonItemGroup, IonLabel, IonNote, IonThumbnail, IonTitle, IonToolbar } from '@ionic/react';
import { informationCircle, star, } from 'ionicons/icons';
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
interface Voiture {
    name: string;
    matricule: string;
    kilometrage: string;
    assurance: string;
}
const voitures: Voiture[] = [
    {
        name: 'Voiture 1',
        matricule: '1413 TBA ',
        kilometrage: '159 KM',
        assurance: '2022/09/12'
    },
];
function FicheTechnique() {
    return (
        <>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        {/* <IonBackButton defaultHref="home" /> */}
                        <IonBackButton defaultHref="/voiture/ListeVoiture" />
                    </IonButtons>
                    <IonTitle>Fiche Technique</IonTitle> 
                </IonToolbar>
            </IonHeader>
            {voitures.map((voiture, index) => {
                return (

                    <IonItem>
                        <IonThumbnail slot="start">
                            <img alt="Silhouette of mountains"
                                src="https://ionicframework.com/docs/img/demos/thumbnail.svg" />
                        </IonThumbnail>
                        <IonLabel>
                            {voiture.name}
                        </IonLabel>
                        <IonLabel>
                            {voiture.matricule}
                        </IonLabel>
                    </IonItem>
                );

            })}
        </>
    );
}
export default FicheTechnique;