
import { useState } from 'react';
// import styled from 'styled-components';
import { IonAvatar, IonButton, IonHeader, IonIcon, IonInput, IonItem, IonItemGroup, IonLabel, IonNote, IonThumbnail, IonTitle, IonToolbar } from '@ionic/react';
import { informationCircle, star } from 'ionicons/icons';
// import './SignIN.css';
function SignUP() {

    const [isTouched, setIsTouched] = useState(false);
    const [isValid, setIsValid] = useState<boolean>();

    const validateEmail = (email: string) => {
        return email.match(
            /^(?=.{1,254}$)(?=.{1,64}@)[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
        );
    };

    const validate = (ev: Event) => {
        const value = (ev.target as HTMLInputElement).value;

        setIsValid(undefined);

        if (value === '') return;

        validateEmail(value) !== null ? setIsValid(true) : setIsValid(false);
    };

    const markTouched = () => {
        setIsTouched(true);
    };

    return (
        <>
            <IonItem lines="none">
                <IonTitle>Sing UP </IonTitle>
            </IonItem>
            <IonItem fill="outline">
                <IonLabel position="floating">Name</IonLabel>
                <IonInput clearInput={true} placeholder="Enter text"></IonInput>
            </IonItem>
            <IonItem fill="solid" className={`${isValid && 'ion-valid'} ${isValid === false && 'ion-invalid'} ${isTouched && 'ion-touched'}`}>
                <IonLabel position="floating">Email</IonLabel>
                <IonInput type="email" clearInput={true} placeholder='Enter email adress' onIonInput={(event) => validate(event)} onIonBlur={() => markTouched()}></IonInput>
            </IonItem>
            <IonItem className='ion-align-items-center ion-justify-content-center' fill="solid">
                <IonLabel position="floating">Password</IonLabel>
                <IonInput type='password' clearInput={true} placeholder="Enter name"></IonInput>
            </IonItem>
            <IonButton routerLink={'/signin/create'} type="submit" expand="block">
                Sign UP
            </IonButton>
        </>
    );
}
export default SignUP;