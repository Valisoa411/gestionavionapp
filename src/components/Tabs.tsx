import React from 'react';
import { IonTabs, IonTabBar, IonTabButton, IonIcon, IonLabel, IonBadge, IonItem, IonTitle, IonMenu, IonButtons, IonMenuButton } from '@ionic/react';
import { calendar, personCircle, map, informationCircle } from 'ionicons/icons';

function TabsExample() {
    return (

        <>
            <IonItem lines="none">
                <IonButtons slot="start">
                    <IonMenuButton></IonMenuButton>
                </IonButtons>

                {/* <IonTabButton tab="about">
                    <IonIcon icon={informationCircle} />
                    <IonLabel>About</IonLabel>
                </IonTabButton>
                <IonTabButton tab="map">
                    <IonIcon icon={map} />
                    <IonLabel>Map</IonLabel>
                </IonTabButton>
                <IonTabButton tab="speakers">
                    <IonIcon icon={personCircle} />
                    <IonLabel>Speakers</IonLabel>
                </IonTabButton> */}
            </IonItem>


            {/* </IonTabBar> */}
            {/* </IonTabs> */}
        </>
    );
}
export default TabsExample;