
import { useState } from 'react';
// import {Button} from Button';
// import styled from 'styled-components';
import { IonAvatar, IonButton, IonButtons, IonHeader, IonIcon, IonInput, IonItem, IonItemGroup, IonLabel, IonMenuButton, IonNote, IonThumbnail, IonTitle, IonToolbar } from '@ionic/react';
import { informationCircle, star, } from 'ionicons/icons';
import { Link } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
interface Voiture {
    name: string;
    url: string;
    matricule: string;
}
const voitures: Voiture[] = [
    {
        name: 'Voiture 1',
        url: '/ficheTechnique/voiture',
        matricule: '1413 TBA '
    },
    {
        name: 'Voiture 2',
        url: '/ficheTechnique/voiture',
        matricule: '1457 BTA'
    },
    {
        name: 'Voiture 3',
        url: '/ficheTechnique/voiture',
        matricule: '1891 GTA'
    }

];
function ListeVoiture() {

    return (
        <>
            <IonItem lines="none">
                <IonButtons slot="start">
                    <IonMenuButton></IonMenuButton>
                </IonButtons>
                <IonTitle>Liste  vehicules </IonTitle>
            </IonItem>
            {voitures.map((voiture, index) => {
                return (

                    <IonItem>
                        <IonThumbnail slot="start">
                            <img alt="Silhouette of mountains"
                                src="https://ionicframework.com/docs/img/demos/thumbnail.svg" />
                        </IonThumbnail>
                        <IonLabel>
                            {voiture.name}
                        </IonLabel>
                        <IonLabel>
                            {voiture.matricule}
                        </IonLabel>
                        <IonItem routerLink={voiture.url} routerDirection="none" lines="none" detail={false} >
                            <IonButton fill="outline" color="warning"  >
                                Consulter
                            </IonButton>
                        </IonItem>

                    </IonItem>
                );

            })}
        </>
    );
}
export default ListeVoiture;