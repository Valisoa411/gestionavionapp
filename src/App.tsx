import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { calendar, car, informationCircle, location, people, statsChartOutline, statsChartSharp } from 'ionicons/icons';
import Menu from './pages/Menu';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import Vehicule from './pages/ListeAvion';
import ListeVehicule from './pages/ListeAvion';
import DetailVehicule from './pages/DetailAvion';
import LoginForm from './pages/LoginForm';
import P_ListeVoiture from './pages/P_ListeVoiture';
import Page from './pages/P_SignUP';
import SignUP from './components/SignUP';
import FicheTechnique from './components/FicheTechnique';
import Login from './components/Login';
import Assurance from './pages/Assurance';
import DetailAvion from './pages/DetailAvion';
import ListeAvion from './pages/ListeAvion';

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonTabs>
        <IonRouterOutlet>
          {/* <Route exact path="/vehicules/:idVehicule">
            <DetailVehicule />
          </Route> */}
          <Route exact path="/avions/:idAvion/:token">
            <DetailAvion />
          </Route>
          <Route exact path="/Assurance">
            <Assurance />
          </Route>
          <Route exact path="/Login">
            <Login />
          </Route>
          <Route exact path="/menu">
            <Menu />
          </Route>
          <Route exact path="/Avion">
            <ListeAvion />
          </Route>
          <Route path="/Menus">
            <Menu />
          </Route>
          <Route exact path="/">
            <Redirect to="/Login" />
          </Route>
        </IonRouterOutlet>

{/* ------------------------------------------------------------------------- */}
        {/* <Menu />
        <IonRouterOutlet id="main">
          <Route path="/" exact={true}>
            <Redirect to="/page/Inbox" />
          </Route>
          <Route path="//ListeVoiture" exact={true}>
            <P_ListeVoiture />
          </Route>
          <Route path="/page/:name" exact={true}>
            <Page />
          </Route>
          <Route path="/signin/create" exact={true}>
            <SignUP />
          </Route>
          <Route path="/ficheTechnique/voiture" exact={true}>
            <FicheTechnique />
          </Route>
        </IonRouterOutlet> */}

{/* ------------------------------------------------------------------------- */}

        <IonTabBar slot="bottom">
          <IonTabButton tab="vehicule" href="/Vehicule">
            <IonIcon icon={car} />
            <IonLabel>Vehicule</IonLabel>
          </IonTabButton>
          <IonTabButton tab="map" href="/Assurance">
            <IonIcon icon={location} />
            <IonLabel>Assurance</IonLabel>
          </IonTabButton>
        </IonTabBar>

      </IonTabs>
    </IonReactRouter>
  </IonApp>
);

export default App;
